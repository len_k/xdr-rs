use byteorder::{BigEndian, ReadBytesExt};
use std::io::prelude::*;
use std::io::SeekFrom;

pub type XDRResult<T> = Result<T, String>;

pub struct XDRReader<T> {
    reader: T,
}

fn as_result<T, U: ToString>(result: Result<T, U>) -> XDRResult<T> {
    result.map_err(|e| e.to_string())
}

impl<T: Read + Seek> XDRReader<T> {
    pub fn new(reader: T) -> XDRReader<T> {
        XDRReader { reader }
    }


    pub fn get_position(&mut self) -> XDRResult<u64> {
        as_result(self.seek(SeekFrom::Current(0)))
    }

    pub fn set_position(&mut self, position: u64) -> XDRResult<u64> {
        as_result(self.seek(SeekFrom::Start(position)))
    }


    pub fn read_int(&mut self) -> XDRResult<i32> {
        as_result(self.reader.read_i32::<BigEndian>())
    }

    pub fn read_uint(&mut self) -> XDRResult<u32> {
        as_result(self.reader.read_u32::<BigEndian>())
    }

    pub fn read_hyper(&mut self) -> XDRResult<i64> {
        as_result(self.reader.read_i64::<BigEndian>())
    }

    pub fn read_uhyper(&mut self) -> XDRResult<u64> {
        as_result(self.reader.read_u64::<BigEndian>())
    }

    pub fn read_float(&mut self) -> XDRResult<f32> {
        as_result(self.reader.read_f32::<BigEndian>())
    }

    pub fn read_double(&mut self) -> XDRResult<f64> {
        as_result(self.reader.read_f64::<BigEndian>())
    }

    pub fn read_string(&mut self) -> XDRResult<String> {
        let length = self.read_uint()? as usize;
        let null_count = (4 - (length % 4)) % 4;
        let mut buffer = vec![0u8; length + null_count];
        self.reader.read(&mut buffer).map_err(|e| e.to_string())?;
        as_result(String::from_utf8(buffer[..length].to_vec()))
    }


    pub fn read_nint(&mut self, n: usize) -> XDRResult<Vec<i32>> {
        (0..n).map(|_| self.read_int()).collect()
    }

    pub fn read_nuint(&mut self, n: usize) -> XDRResult<Vec<u32>> {
        (0..n).map(|_| self.read_uint()).collect()
    }

    pub fn read_nhyper(&mut self, n: usize) -> XDRResult<Vec<i64>> {
        (0..n).map(|_| self.read_hyper()).collect()
    }

    pub fn read_nuhyper(&mut self, n: usize) -> XDRResult<Vec<u64>> {
        (0..n).map(|_| self.read_uhyper()).collect()
    }

    pub fn read_nfloat(&mut self, n: usize) -> XDRResult<Vec<f32>> {
        (0..n).map(|_| self.read_float()).collect()
    }

    pub fn read_ndouble(&mut self, n: usize) -> XDRResult<Vec<f64>> {
        (0..n).map(|_| self.read_double()).collect()
    }

    pub fn read_nstring(&mut self, n: usize) -> XDRResult<Vec<String>> {
        (0..n).map(|_| self.read_string()).collect()
    }
}

impl<T: Seek> Seek for XDRReader<T> {
    fn seek(&mut self, pos: SeekFrom) -> std::io::Result<u64> {
        self.reader.seek(pos)
    }
}
